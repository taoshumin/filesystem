module gitlab.com/taoshumin/filesystem

go 1.16

require (
	github.com/fsnotify/fsnotify v1.5.1
	github.com/spf13/afero v1.6.0
)
