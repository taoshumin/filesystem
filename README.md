# filesystem

简介：Afero是一个文件系统框架，提供了一个简单、统一和通用的API与任何文件系统交互，作为一个抽象层提供接口、类型和方法。Afero有一个非常干净的接口和简单的设计，没有不必要的构造函数或初始化方法。跨平台支持window和linux。

### 一、安装

```shell
go get github.com/spf13/afero
```

### 二、使用

```shell
os.Open("/tmp/foo") 
```


### 三、filepath使用

```
filepath.Join(path,"*proto")
```





